module.exports = {
    getPrimaryAttributes: (generatedCharacter) => {
        let charachterCopy = generatedCharacter
        let primaryAttributes = (( {combat, brawn, agility, perception, intelligence, willpower, fellowship}) => (
                {combat, brawn, agility, perception,intelligence, willpower, fellowship})
        )(charachterCopy)
        return primaryAttributes
    },

    getUpbringing: (generatedCharacter) => {
        let charachterCopy = generatedCharacter
        let upbringing = (( {upbringing}) => (
                {upbringing})
        )(charachterCopy)
        return upbringing
    },//getFavouritePrimaryAttribute

    getPrimaryAttributesBonuses: (generatedCharacter) => {
        let charachterCopy = generatedCharacter;
        let primaryAttributesBonuses = (({combatBonus, brawnBonus, agilityBonus, perceptionBonus, intelligenceBonus, willpowerBonus, fellowshipBonus}) => (
                {combatBonus, brawnBonus, agilityBonus, perceptionBonus, intelligenceBonus, willpowerBonus, fellowshipBonus})
        )(charachterCopy)
        return primaryAttributesBonuses
    },

    getAlignment: (generatedCharacter) => {
        let charachterCopy = generatedCharacter
        let alignment = (({chaosAlignment, orderAlignment}) => (
                {chaosAlignment, orderAlignment})
        )(charachterCopy)
        return alignment
    },

    getGeneralData: (generatedCharacter) => {
        let charachterCopy = generatedCharacter;
        let generalData = (({name, ancestry, sex, ageGroup,  profession, upbringing, socialClass, seasonOfBirth}) => (
                {name, ancestry, sex, ageGroup, profession, upbringing, socialClass, seasonOfBirth})
        )(charachterCopy);
        return generalData;
    },

    getDescriptiveTraits: (generatedCharacter) => {
        let charachterCopy = generatedCharacter;
        let descriptiveTrait = (({ dooming, ancestralTrait, drawback}) => ({dooming, ancestralTrait, drawback}))(charachterCopy);
        return descriptiveTrait;
    },

    getCharacterAppearance: (generatedCharacter) => { // to refactor better
        let charachterCopy = generatedCharacter
        let characterAppearance;
        if(generatedCharacter.distinguishingMarks.length === 0) {
             characterAppearance = (({ buildType, complexion, distinguishingMarks, eyeColor, hairColor, height, weight}) => (
                    { buildType, complexion, distinguishingMarks, eyeColor, hairColor, height, weight})
            )(charachterCopy)
        } else {
            characterAppearance = (({ buildType, complexion, eyeColor, hairColor, height, weight}) => (
                    { buildType, complexion, eyeColor, hairColor, height, weight})
            )(charachterCopy)
        }

        return characterAppearance
    },

    getGeneralPoints: (generatedCharacter) => {
        let charachterCopy = generatedCharacter
        let generalPoints = (( { damageThreshold, encumbranceLimit, fatePoints, initiative, movement, perilThreshold } ) => (
                { damageThreshold, encumbranceLimit, fatePoints, initiative, movement, perilThreshold } )
        )(charachterCopy)
        return generalPoints
    },

    getDrawback: (generatedCharacter) => {
        let charachterCopy = generatedCharacter
        let drawback = (( { drawback } ) => (
                { drawback } )
        )(charachterCopy)
        return drawback
    },

}
