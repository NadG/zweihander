import React from 'react';
import './Zweihander.css';
import {BrowserRouter} from 'react-router-dom'
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import Footer from "components/Footer/Footer";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <div className="Zweihander">
        <Header/>
        <Home/>
        <Footer/>
      </div>
    </BrowserRouter>
  );
}

export default App;
