import axios from 'axios';

const baseURL = 'https://zweihander.herokuapp.com/';

axios.defaults.headers.post['Content-Type'] = 'application/json';
const client = axios.create({
  baseURL,
  withCredentials: true,
});

export function login() {
  return client.post('/characters/', {});
}

export async function get<Request, Response>(
  url: string,
  data: Request,
): Promise<Response> {
  const response = await client.get<Response>(url, {
    data,
  });

  return response.data;
}

export async function post<Request, Response>(
  url: string,
  data: Request,
): Promise<Response> {
  const response = await client.post<Response>(url, data, {});

  return response.data;
}
