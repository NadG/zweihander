import React from 'react';
import header from './header.module.scss'
import {NavLink} from "react-router-dom";
import classnames from 'classnames';

const Header = () => {

  return (
    <header className={classnames(header.header, header.border)}>
      <NavLink to="/" className={header.logo}>
        <h1>Zweihander</h1>
      </NavLink>
    </header>
  )
}

export default Header;
