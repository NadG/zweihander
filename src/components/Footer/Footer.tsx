import React from 'react';
import footer from './footer.module.scss'
import classnames from 'classnames';
import {ReactComponent as Spade} from 'assets/icons/dividers/div-24.svg';

const Footer = () => {
  return (
    <footer className={footer.footer}>
      <div className={footer.spade_container}>
        <Spade className={footer.spade}/>
      </div>
      <div className={footer.creators}>
        Coded with ❤ by <a href="https://www.linkedin.com/in/nadia-guarracino17/" target="_blank"> Nadia
        Guarracino </a>
        and <a href="https://www.linkedin.com/in/michele-sangalli-75b91a7a/" target="_blank"> Michele Sangalli </a>
        <br/>
        Thanks to <a href="https://www.freepik.com/free-photos-vectors/background"> www.freepik.com</a> for icons and
        frames
      </div>
    </footer>
  )
}

export default Footer;
