import React, {useEffect, useState} from 'react';
import axios from 'axios'
import Detail from "components/CharacterList/Detail/Detail";
import {getFormattedSex} from 'utils/dataDisplayMethods'
import {RouteComponentProps} from "react-router";
import list from './characterList.module.scss'

type CharacterListResponse = {
  ageGroup: string;
  ancestry: string;
  buildType: object;
  id: number;
  name: string;
  profession: string;
  sex: 'm' | 'f';
}

const CharacterList: React.FC<{} & RouteComponentProps<{}>> = () => {
  const [characterList, setCharacterList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios.get('https://zweihander.herokuapp.com/characters/').then((res: any) => {
      setCharacterList(res.data);
      setIsLoading(false);
      console.log(res.data);
    }).catch((err: any) => {
      console.log(err);
    });
  }, []);

  function renderList() {
    let characterListData = characterList;

    if (!isLoading) {
      return characterListData.map((el: any, i: number) => {
          return (
            <Detail
              name={el.name}
              ancestry={el.ancestry}
              buildType={el.buildType.name}
              sex={getFormattedSex(el.sex)}
              profession={el.profession}
              key={i}
              id={el.id}
              // view={this.handleViewCharacterSelected}
            />
          )
        }
      )
    }
  }

  return (
    <div className={list.container}>
      <div className={list.wrapper}>
        <h1 className={list.page_title}>Characters saved</h1>
        <br/>
        {isLoading && <p>Loading saved characters...</p>}
        <div className={list.card_wrapper}>
          {renderList()}
        </div>
      </div>
    </div>
  )
}

export default CharacterList;
