import React from 'react';
import detail from './detail.module.scss';
import {NavLink} from "react-router-dom";

interface DetailProps {
  id: number;
  name: string;
  ancestry: string;
  sex: string;
  buildType: string;
  profession: string;
}

const Detail: React.FC<DetailProps> = ({id, name, ancestry, profession, buildType, sex}) => {
  return (
    <div className={detail.item}>
      <div>
        <h3>{name}</h3>
        <h4> {ancestry}</h4>
        <br/>
        <p>{sex}, {buildType}, {profession} </p>
      </div>
      <br/>
      <div className={detail.button_group}>
        <button className={detail.button}>Delete</button>

        <NavLink to={'/character-sheet/' + id}>
          <button className={detail.button}>View</button>
        </NavLink>
      </div>
    </div>
  )
}

export default Detail;
