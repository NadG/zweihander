import React from 'react'

interface BoxProps {
  title: string;
  value: number;
  bonus?: number;
}

const Box: React.FC<BoxProps> = ({title, value, bonus}) => {
  return (
    <div>
      <h4>{title}</h4>
      <h2>{value}</h2>
      {bonus ? <p>bonus: <span>{bonus}</span></p> : null}
    </div>
  )
}

export default Box;
