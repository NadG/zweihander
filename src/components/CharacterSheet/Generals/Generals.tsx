import React from 'react';
import detail from './detail.module.scss';
import {NavLink} from "react-router-dom";

interface GeneralsProps {
  data: any;
  upbringing: any;
}

const Generals: React.FC<GeneralsProps> = ({data, upbringing}) => {
  console.log(data);
  return (
    <div>
      <div>
          <span>
              <h1>{data.name}</h1>
              <h4> {data.ancestry}</h4>
          </span>
      </div>
      <p>{data.sex}, {data.ageGroup.toLowerCase()}, {data.socialClass.toLowerCase()}, {data.profession.toLowerCase()} </p>
      <br/>
      <ul>
        <li>
          <p>Born in <span>{data.seasonOfBirth}</span></p>
        </li>
        <li>
          <p>Upbringing: <span>{upbringing.name}</span></p>
        </li>
        <li>
          <p>Favoured primary
            attribute: <span>{upbringing.favoredPrimaryAttribute}</span></p>
        </li>
      </ul>
      <br/>
    </div>
  )
}

export default Generals;
