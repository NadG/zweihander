import React from 'react'
import Box from "../Box/Box";
import {getFormattedText} from 'utils/dataDisplayMethods'
import points_style from './points.module.scss'

interface PointsProps {
  points: any;
  bonuses?: any;
  forAlignment?: string;
  upbringing?: any;
}

const Points: React.FC<PointsProps> = ({points, bonuses, forAlignment, upbringing}) => {
  function renderPoints() {
    let pointsArr: any = [];
    for (const [key, value] of Object.entries(points)) {
      if (value !== null) {
        let a;
        a = {
          title: key,
          value: value,
          bonus: null
        }
        // if there are bonuses
        let newkey = a.title + "Bonus";
        if (bonuses && newkey in bonuses) {
          a.bonus = bonuses[newkey]
        }
        pointsArr.push(a)
      }
    }
    return (
      renderPointsBoxes(pointsArr)
    )
  }

  function renderPointsBoxes(arr: any) {
    return arr.map((e: any, i: number) => {
      let favoredPA;
      if (upbringing) {
        favoredPA = upbringing.upbringing.favoredPrimaryAttribute.toLowerCase();
        return (
          <div className={favoredPA.trim() === e.title.trim() ? points_style.accent_border : points_style.border}
               key={i}>
            <Box title={getFormattedText(e.title)} value={e.value} bonus={e.bonus}/>
          </div>
        )
      } else {
        return <Box title={getFormattedText(e.title)} value={e.value} bonus={e.bonus} key={i}/>
      }
    })
  }

  return (
    <div className={points_style.container}>
      {renderPoints()}
    </div>
  )
}

export default Points;
