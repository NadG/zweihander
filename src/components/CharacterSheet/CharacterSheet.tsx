import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {
  getPrimaryAttributes,
  getPrimaryAttributesBonuses,
  getUpbringing,
  getCharacterAppearance,
  getGeneralData,
  getAlignment,
  getGeneralPoints
} from 'utils/utils'
import {getFormattedText} from 'utils/dataDisplayMethods'
import Points from "components/CharacterSheet/Points/Points";
import Generals from "components/CharacterSheet/Generals/Generals";
import {is} from "@babel/types";

interface RouterProps {
  match: any;
}

type Props = RouterProps;


const CharacterSheet: React.FC<RouterProps> = ({match}: Props) => {
  const selectedCharacterId = match.params.id;
  const [sheetData, setSheetData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios.get('https://zweihander.herokuapp.com/characters/' + selectedCharacterId).then((res: any) => {
      setSheetData(res.data);
      setIsLoading(false);
    }).catch((err: any) => {
      console.log(err);
    });
  }, []);

  function renderGeneralData() {
    if (!isLoading) {
      return <Generals
        data={getGeneralData(sheetData)}
        upbringing={getUpbringing(sheetData)}
      />
    }
  }

  function renderPrimaryAttributes() {
    if (!isLoading) {
      return (
        <Points
          points={getPrimaryAttributes(sheetData)}
          bonuses={getPrimaryAttributesBonuses(sheetData)}
          upbringing={getUpbringing(sheetData)}
        />
      )
    }
  }

  function renderGeneralPoints() {
    return <Points points={getGeneralPoints(sheetData)}/>
  }

  function renderAlignment() {
    return <Points points={getAlignment(sheetData)}/>
  }

  function renderDescriptiveTraits() {
    if (!isLoading) {
      return (
        <div>
          <br/>
          {/*<DescriptiveTrait*/}
          {/*  traitType="Ancestral trait"*/}
          {/*  traitName={sheetData.ancestralTrait.name}*/}
          {/*  traitEffect={characterSheetData.ancestralTrait.effect}/>*/}
          {/*<br/>*/}
          {/*<DescriptiveTrait*/}
          {/*  traitType="Dooming"*/}
          {/*  traitEffect={characterSheetData.dooming}/>*/}
          {/*<br/>*/}
          {/*{sheetData.drawback ?*/}
          {/*  <DescriptiveTrait traitType="Drawback" traitName={characterSheetData.drawback.name}*/}
          {/*                    traitEffect={characterSheetData.drawback.effect}/> : null}*/}
        </div>
      )
    }
    }
    return (
      <div>
        {renderGeneralData()}
        <br/>
        {renderAlignment()}
        <br/>
        {renderPrimaryAttributes()}
        <br/>
        {renderGeneralPoints()}
      </div>
    )
  }

  export default CharacterSheet;
