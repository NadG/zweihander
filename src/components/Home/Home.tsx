import React from 'react';
import home from './home.module.scss';
import {Switch, Route} from "react-router-dom"
import CharacterList from "components/CharacterList/CharacterList";
import CharacterSheet from "components/CharacterSheet/CharacterSheet";

interface HomeProps {

}

const Home: React.FC<HomeProps> = () => {
  return (
    <div className={home.home}>
      <Switch>
        <Route exact path="/" component={CharacterList}/>
        <Route path="/character-sheet/:id?" component={CharacterSheet}/>
        {/*<Route path="/character-sheet/view-character/:id?" component={CharacterSheet}/>*/}
        {/*<Route path="/character-sheet/:options?" component={CharacterSheet}/>*/}
      </Switch>
    </div>
  )
}

export default Home;
